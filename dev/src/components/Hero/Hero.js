import $ from 'jquery'
import 'slick-carousel'

const $slider = $('.hero__slider')
const $sliderWrapper = $slider.find('.hero__slider-wrapper')

$sliderWrapper.slick({
    prevArrow: $slider.find('.hero__control--prev'),
    nextArrow: $slider.find('.hero__control--next')
})