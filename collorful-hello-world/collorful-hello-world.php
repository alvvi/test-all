<?php 
/*
Plugin Name: Colorful Hello World
Description: Adds Colroful Hello World widget
Version: 1.0.0
*/

if( ! defined('ABSPATH') ) {
    exit();
}

function chw_add_scripts() {
    wp_enqueue_style( 'chw-styles', plugins_url().'/collorful-hello-world/css/index.css' );
}
add_action( 'wp_enqueue_scripts', 'chw_add_scripts' );

require_once( plugin_dir_path( __FILE__ ).'/inc/class.hello-world-widget.php' );
add_action( 'widgets_init', function() {
    register_widget( 'Hello_World_Widget' );
});