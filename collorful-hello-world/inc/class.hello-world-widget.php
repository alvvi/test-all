<?php

class Hello_World_Widget extends WP_Widget {

    /**
     * Sets up the widgets name etc
     */
    public function __construct() {
        $widget_ops = array( 
            'classname' => 'my_widget',
            'description' => 'Say hello',
        );
        parent::__construct( 'hello_world_widget', 'Colorful Hello World', $widget_ops );
    }

    /**
     * Returns a class depending of time of the day
     * @return string
     */
    public static function get_text_class() {
        $time = date("H");
        $timezone = date("e");

        if ($time < "12") {
            return "time--morning";
        } elseif ($time >= "12" && $time < "17") {
            return "time--afternoon";
        } elseif ($time >= "17" && $time < "19") {
            return "time--evening";
        } elseif ($time >= "19") {
            return "time--night";
        }
    }

    /**
     * Outputs the content of the widget
     *
     * @param array $args
     * @param array $instance
     */
    public function widget( $args, $instance ) {
        
        $time_class = $this->get_text_class();
        ?>
            <h4 class="time <?php echo $time_class; ?>">Hello world!</h4>
        <?php 
    }

    /**
     * Outputs the options form on admin
     *
     * @param array $instance The widget options
     */
    public function form( $instance ) {
        // outputs the options form on admin
        ?>
        <p>Hello world</p>
        <?php
    }

    /**
     * Processing widget options on save
     *
     * @param array $new_instance The new options
     * @param array $old_instance The previous options
     *
     * @return array
     */
    public function update( $new_instance, $old_instance ) {
        // processes widget options to be saved
    }
}