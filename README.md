# Test Wordpress Theme

## Getting started

First, clone the repo and put the contents in /wp-content/themes folder

If the `/test` folder is empty, run `git submodule update --init --recursive` to fetch the submodule files

Then inside the theme(`/test`) folder open command line and run first `npm install`, then `npm run prod` to build themes assets or `npm run dev` to start gulp in the developing mode

All not "combined" gulp tasks are located in `/tasks` subfolder, to see *all* tasks run `gulp --tasks`
The source files could be found in `/dev` as requested in the assignment, but also in `/test/src`, these are used for the building process

## Folders

`/test` folder contains the **theme**

`/test/src` folder contains assets and source files, they are processed by gulp
`/test/src/styles` folder contains styles shared between pages, vendor styles get included from `node_modules` directly
`/test/src/pages` folder contains .js and .scss files for pages
`/test/src/componetns` folder contains .js and .scss files for individual components, and assets for them if necessary

`/test/public` folder contains static files

`/test-child` folder contains **the child theme**

`/colorful-hello-world` folder contains the **plugin** mentioned in the assignment, copy it to /wp-content/plugins and activate the plugin in the menu